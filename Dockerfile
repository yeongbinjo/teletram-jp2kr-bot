FROM alpine:latest

ENV LANG=ko_KR.UTF-8 \
    LANGUAGE=ko_KR.UTF-8 \
    LC_CTYPE=ko_KR.UTF-8 \
    LC_ALL=ko_KR.UTF-8
ARG SECRET_KEY
ARG ALLOWED_HOSTS
ENV SECRET_KEY=$SECRET_KEY
ENV ALLOWED_HOSTS=$ALLOWED_HOSTS
ENV DATABASE_URL=$DATABASE_URL

RUN apk add --no-cache \
        alpine-sdk \
        linux-headers \
        mailcap \
        libffi-dev \
        jpeg-dev \
        zlib-dev \
        postgresql-dev \
        postgresql \
        yarn \
        python3-dev \
        python3
RUN pip3 install -U pip setuptools
RUN pip3 install pipenv

EXPOSE 8000

COPY ./Pipfile /app/Pipfile
COPY ./Pipfile.lock /app/Pipfile.lock
WORKDIR /app
RUN pipenv install --deploy --system

COPY . /app
WORKDIR /app/telegram_jp2kr_bot
RUN python3 manage.py collectstatic --noinput
CMD ["uwsgi", "--http", ":8000", \
    "--mimefile", "/etc/mime.types", \
    "--module", "telegram-jp2kr-bot.wsgi"]
