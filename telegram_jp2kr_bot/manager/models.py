import uuid
from django.db import models


class AuthToken(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    description = models.TextField(blank=True)
    active = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.id} ({self.description})'


class AuthenticatedChatId(models.Model):
    auth_token = models.ForeignKey(AuthToken, on_delete=models.CASCADE)
    chat_id = models.BigIntegerField(unique=True)
    chat_title = models.TextField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.chat_title} ({self.auth_token.description})'


class Log(models.Model):
    chat_id = models.BigIntegerField()
    chat_title = models.TextField(blank=True, null=True)
    telegram_user_id = models.BigIntegerField()
    telegram_user_name = models.TextField()
    message = models.TextField(blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'[{self.chat_title}] {self.telegram_user_name}: {self.message}'


class UserLanguageCode(models.Model):
    telegram_user_id = models.BigIntegerField(unique=True)
    telegram_user_name = models.TextField()
    language_code = models.CharField(max_length=2)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.telegram_user_name} ({self.language_code})'
