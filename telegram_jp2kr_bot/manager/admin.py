from django.contrib import admin

from manager.models import AuthToken, AuthenticatedChatId, Log, UserLanguageCode


@admin.register(AuthToken)
class AuthTokenAdmin(admin.ModelAdmin):
    list_display = ['id', 'description', 'active', 'created_at']


@admin.register(AuthenticatedChatId)
class AuthTokenAdmin(admin.ModelAdmin):
    list_display = ['auth_token', 'chat_title', 'created_at', 'updated_at']


@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    list_display = ['chat_title', 'telegram_user_name', 'message', 'created_at']


@admin.register(UserLanguageCode)
class UserLanguageCodeAdmin(admin.ModelAdmin):
    list_display = ['telegram_user_name', 'language_code', 'created_at']
