# coding: utf-8

import logging
import os
import json
import urllib.request
import urllib.parse
from urllib.error import HTTPError
from uuid import uuid4

import django
import environ
from django.conf import settings

from telegram import InputTextMessageContent, InlineQueryResultArticle, ParseMode
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, InlineQueryHandler


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

env = environ.Env()
settings.configure(
    DATABASES={
        'default': env.db('DATABASE_URL')
    },
    INSTALLED_APPS=[
        'manager',
    ]
)
django.setup()


def translate(from_code, to_code, text):
    if not text:
        return
    client_id = os.environ.get('NAVER_API_CLIENT_ID', '')
    client_secret = os.environ.get('NAVER_API_CLIENT_SECRET', '')
    data = f"source={from_code}&target={to_code}&text={urllib.parse.quote(text)}"
    url = "https://openapi.naver.com/v1/papago/n2mt"
    request = urllib.request.Request(url)
    request.add_header("X-Naver-Client-Id", client_id)
    request.add_header("X-Naver-Client-Secret", client_secret)
    response = urllib.request.urlopen(request, data=data.encode("utf-8"))
    rescode = response.getcode()
    if rescode == 200:
        response_body = response.read()
        return json.loads(response_body.decode('utf-8'))['message']['result']['translatedText']
    else:
        return


def get_to_code(from_code):
    if from_code == 'ko':
        return 'ja'
    elif from_code == 'ja':
        return 'ko'
    else:
        return 'ko'


def get_prefix(to_code):
    return '自動翻訳: ' if to_code == 'ja' else '자동번역: '


def get_telegram_user_name(user):
    if user.username:
        return user.username

    if user.first_name and user.last_name:
        return f'{str(user.first_name)} {str(user.last_name)}'
    elif user.last_name:
        return user.last_name
    else:
        return user.first_name


def korean(bot, update):
    if update.message is None:
        return

    from manager.models import UserLanguageCode
    user_language_code, created = UserLanguageCode.objects.update_or_create(
        telegram_user_id=update.message.from_user.id,
        defaults={
            'telegram_user_name': get_telegram_user_name(update.message.from_user),
            'language_code': 'ko'
        }
    )
    bot.send_message(
        chat_id=update.message.chat.id,
        text=f'{user_language_code.telegram_user_name}님의 언어 설정이 업데이트 되었습니다'
    )


def japanese(bot, update):
    if update.message is None:
        return

    from manager.models import UserLanguageCode
    user_language_code, created = UserLanguageCode.objects.update_or_create(
        telegram_user_id=update.message.from_user.id,
        defaults={
            'telegram_user_name': get_telegram_user_name(update.message.from_user),
            'language_code': 'ja'
        }
    )
    bot.send_message(
        chat_id=update.message.chat.id,
        text=f'{user_language_code.telegram_user_name}さんの言語設定が更新されました。'
    )


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    if update.message is None:
        return

    from manager.models import AuthToken
    from django.core.exceptions import ValidationError
    try:
        auth_token_id = update.message.text.split()[1]
        auth_token = AuthToken.objects.get(id=auth_token_id)
    except IndexError:
        bot.send_message(
            chat_id=update.message.chat.id,
            text='인증 토큰을 입력해주세요'
        )
        return
    except (ValueError, ValidationError, AuthToken.DoesNotExist):
        bot.send_message(
            chat_id=update.message.chat.id,
            text='존재하지 않는 인증 토큰입니다'
        )
        return

    if not auth_token.active:
        bot.send_message(
            chat_id=update.message.chat.id,
            text='만료된 인증 토큰입니다'
        )
        return

    from manager.models import AuthenticatedChatId
    _, created = AuthenticatedChatId.objects.get_or_create(
        chat_id=update.message.chat.id,
        defaults={
            'auth_token': auth_token,
            'chat_title': update.message.chat.title
        }
    )
    if created:
        bot.send_message(
            chat_id=update.message.chat.id,
            text=f'인증되었습니다'
        )
    else:
        bot.send_message(
            chat_id=update.message.chat.id,
            text=f'이미 인증된 채널입니다'
        )

    bot.send_message(
        chat_id=update.message.chat.id,
        text=f'한일 번역 봇 jp2kr 준비 완료됐습니다.\n日韓翻訳ボットjp2kr準備完了されました。',
        parse_mode=ParseMode.MARKDOWN
    )


def echo(bot, update):
    """Echo the user message."""
    if update.message is None:
        return

    from manager.models import Log
    Log.objects.create(
        chat_id=update.message.chat.id,
        chat_title=update.message.chat.title,
        telegram_user_id=update.message.from_user.id,
        telegram_user_name=get_telegram_user_name(update.message.from_user),
        message=update.message.text
    )

    from manager.models import AuthenticatedChatId
    try:
        authenticated_chat_id = AuthenticatedChatId.objects.get(
            chat_id=update.message.chat.id)
    except AuthenticatedChatId.DoesNotExist:
        return
    if not authenticated_chat_id.auth_token.active:
        return

    if update.message.text.find('自動翻訳') != 0 and update.message.text.find('자동번역') != 0:
        from manager.models import UserLanguageCode
        try:
            user_language_code = UserLanguageCode.objects.get(telegram_user_id=update.message.from_user.id)
            from_code = user_language_code.language_code
        except UserLanguageCode.DoesNotExist:
            from_code = 'ja'
        to_code = get_to_code(from_code)
        translated_message = translate(from_code, to_code, update.message.text)
        update.message.reply_text(get_prefix(to_code) + translated_message)


def inline(bot, update):
    query = update.inline_query.query
    if query:
        results = [
            InlineQueryResultArticle(
                id=uuid4(),
                title="한국어 -> 日本語",
                input_message_content=InputTextMessageContent(
                    get_prefix('ja') + str(translate('ko', 'ja', query)))),
            InlineQueryResultArticle(
                id=uuid4(),
                title="日本語 -> 한국어",
                input_message_content=InputTextMessageContent(
                    get_prefix('ko') + str(translate('ja', 'ko', query)))),
        ]
    else:
        results = []

    update.inline_query.answer(results)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(os.environ.get('TELEGRAM_BOT_TOKEN'))

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("korean", korean))
    dp.add_handler(CommandHandler("japanese", japanese))
    dp.add_handler(InlineQueryHandler(inline))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()

